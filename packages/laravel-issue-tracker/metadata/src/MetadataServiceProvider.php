<?php

namespace LaravelIssueTracker\Metadata;

use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class MetadataServiceProvider extends ServiceProvider {

    /**
     * @var array
     */
    static $scopes = ['read-metadata'    => 'Read all metadata',
                      'store-metadata'   => 'Store new metadata',
                      'update-metadata'  => 'Update an existing metadata',
                      'destroy-metadata' => 'Destroy an existing metadata'];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        $this->loadMigrationsFrom(__DIR__ . '/migrations');

        if ( ! $this->app->routesAreCached())
        {
            require __DIR__ . '/routes.php';
        }

        Passport::$scopes = array_merge(Passport::$scopes, self::$scopes);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}